import React, { Component } from "react";
import Axios from "axios";
export default class Todolist extends Component {
  state = {
    taskList: [],
    taskName: "",
  };
  getTaskList = () => {
    let promise = Axios({
      url: "http://localhost:8080/api/todo",
      method: "GET",
    });
    promise
      .then((result) => {
        console.log(result.data);
        // console.log(this.state.taskId);
        this.setState({
          taskList: result.data,
        });
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };
  renderTaskToDo = () => {
    return this.state.taskList
      .filter((item) => !item.status)
      .map((item, index) => {
        return (
          <li key={index}>
            <span>{item.title}</span>
            <div className="buttons">
              <button
                className="remove"
                type="button"
                onClick={() => {
                  this.deleteTask(item.id);
                }}
              >
                <i className="fa fa-trash-alt" />
              </button>
              <button
                className="complete"
                type="button"
                onClick={() => {
                  this.doneTask(item.id);
                  console.log(item.id);
                }}
              >
                <i className="far fa-check-circle" />
                <i className="fas fa-check-circle" />
              </button>
            </div>
          </li>
        );
      });
  };
  renderTaskToDoDone = () => {
    return this.state.taskList
      .filter((item) => item.status)
      .map((item, index) => {
        return (
          <li key={index}>
            <span>{item.title}</span>
            <div className="buttons">
              <button
                className="remove"
                type="button"
                onClick={() => {
                  this.deleteTask(item.id);
                  console.log(item.id);
                }}
              >
                <i className="fa fa-trash-alt" />
              </button>
              <button className="complete" type="button">
                <i className="far fa-check-circle" />
                <i className="fas fa-check-circle" />
              </button>
            </div>
          </li>
        );
      });
  };
  doneTask = (taskId) => {
    let promise = Axios({
      url: `http://localhost:8080/api/todo/done/${taskId}`,
      method: "PUT",
    });
    promise
      .then((result) => {
        console.log(result.data);
        this.getTaskList();
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };
  deleteTask = (taskId) => {
    let promise = Axios({
      url: `http://localhost:8080/api/todo/${taskId}`,
      method: "DELETE",
    });
    promise
      .then((result) => {
        console.log(result.data);
        this.getTaskList();
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };
  componentDidMount() {
    this.getTaskList();
  }

  handleChange = (e) => {
    let { value, name } = e.target;
    console.log(value, name);
    this.setState({
      taskName: value,
    });
  };

  addTask = (e) => {
    e.preventDefault();

    let promise = Axios({
      url: "http://localhost:8080/api/todo",
      method: "POST",
      data: { title: this.state.taskName },
    });
    promise
      .then((result) => {
        console.log(result.data);
        this.getTaskList();
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };
  getDate = () => {
    let d = new Date();
    return d.toUTCString();
  };
  render() {
    return (
      <form onSubmit={this.addTask}>
        <div className="card">
          <div className="card__header">
            <img src={require("./bg.png")} />
          </div>
          <div className="card__body">
            <div className="card__content">
              <div className="card__title">
                <h2>My Tasks</h2>
                <p>{this.getDate()}</p>
              </div>
              <div className="card__add">
                <input
                  name="taskName"
                  onChange={this.handleChange}
                  id="newTask"
                  type="text"
                  placeholder="Enter an activity..."
                />
                {/* <p className="text text-danger">{this.state.errors.taskName}</p> */}
                <button id="addItem" onClick={this.addTask}>
                  <i className="fa fa-plus" />
                </button>
              </div>
              <div className="card__todo">
                {/* Uncompleted tasks */}
                <ul className="todo" id="todo">
                  {this.renderTaskToDo()}
                  {/* <li>
                    <span>Đi ngủ</span>
                    <div className="buttons">
                      <button className="remove">
                        <i className="fa fa-trash-alt" />
                      </button>
                      <button className="complete">
                        <i className="far fa-check-circle" />
                        <i className="fas fa-check-circle" />
                      </button>
                    </div>
                  </li> */}
                </ul>
                {/* Completed tasks */}
                <ul className="todo" id="completed">
                  {this.renderTaskToDoDone()}

                  {/* <li>
                    <span>Ăn sáng</span>
                    <div className="buttons">
                      <button className="remove">
                        <i className="fa fa-trash-alt" />
                      </button>
                      <button className="complete">
                        <i className="far fa-check-circle" />
                        <i className="fas fa-check-circle" />
                      </button>
                    </div>
                  </li> */}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
